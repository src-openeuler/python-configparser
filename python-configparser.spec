%global _description \
The ancient ConfigParser module available in the standard library 2.x \
has seen a major update in Python 3.2. This package is a backport of \
those changes so that they can be used directly in Python 2.6 - 3.5.

Name:           python-configparser
Version:        7.1.0
Release:        1
Summary:        Configuration file parser for python
License:        MIT
URL:            https://bitbucket.org/ambv/configparser
Source0:        %{pypi_source configparser}

BuildArch:      noarch
BuildRequires:  python3-devel python3-setuptools
BuildRequires:  python3-pip python3-wheel python3-pytest
BuildRequires:  python3-hatch-vcs
Requires:       python3-setuptools

%description %_description

%package -n python3-configparser
Summary:        Configuration file parser for python
%{?python_provide:%python_provide python3-configparser}

%description -n python3-configparser %_description

%prep
%autosetup -n configparser-%{version} -p1
rm -rf *.egg-info

%build
%pyproject_build

%install
%pyproject_install

%check
export PYTHONPATH=%{buildroot}%{python3_sitelib}
pytest

%files -n python3-configparser
%doc README.rst
%{python3_sitelib}/*

%changelog
* Thu Oct 24 2024 Ge Wang <wang__ge@126.com> - 7.1.0-1
- Update to 7.1.0

* Thu Jun 01 2023 yaoxin <yao_xin001@hoperun.com> - 5.3.0-1
- Update to 5.3.0

* Tue Jun 07 2022 SimpleUpdate Robot <tc@openeuler.org> - 5.2.0-1
- Upgrade to version 5.2.0

* Wed Oct 21 2020 chengzihan <chengzihan2@huawei.com> - 3.5.0b2-12
- Remove subpackage python2-configparser

* Fri Nov 15 2019 sunguoshuai <sunguoshuai@huawei.com> - 3.5.0b2-11
- Package init
